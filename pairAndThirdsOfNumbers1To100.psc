Algoritmo pairAndThirdsOfNumbers1To100
	contador<-0
	Repetir
		//hacer variar el contador para que el ciclo finalice despu�s de un n�mero
		//espec�fico de iteraciones
		contador<-contador+1
		Si contador%2==0 Entonces
			Escribir contador, " es multiplo de dos"
		Fin Si
		
		Si contador%3==0 Entonces
			Escribir contador, " es multiplo de tres"
		FinSi
	Hasta Que contador==100
	
FinAlgoritmo
